const submitData = e => {

    let formData = JSON.parse(localStorage.getItem('data')) || [];

    formData.push({

        Name: document.getElementById("name").value,
        Phone: document.getElementById("phone").value,
        Email: document.getElementById("email").value,
        Address: document.getElementById("address").value,

    });

    localStorage.setItem('data', JSON.stringify(formData))


    tableData();
    e.preventDefault();
}


function tableData() {

    // console.log(localStorage.getItem('data'));

    if (localStorage.getItem('data')) {

        // console.log(localStorage.getItem('formData'));

        const output = document.querySelector('tbody');

        output.innerHTML = "";

        JSON.parse(localStorage.getItem('data')).forEach(data => {

            output.innerHTML += `

                    <tr>
                    <td>${data.Name}</td>
                    <td>${data.Phone}</td>
                    <td>${data.Email}</td>
                    <td>${data.Address}</td>
                    </tr>`;
        });
    }
}

tableData()


$('th').on('click', function() {

    let column = $(this).data('column');
    let order = $(this).data('order');
    // console.log("clikced", column, order);


    let mydata = JSON.parse(localStorage.getItem('data'));



    if (order == "desc") {
        $(this).data("order", "asc");
        mydata = mydata.sort((a, b) => a[column] > b[column] ? 1 : -1);
    } else {
        $(this).data("order", "desc");
        mydata = mydata.sort((a, b) => a[column] < b[column] ? 1 : -1);
    }

    //console.log(mydata);


    tableData1(mydata);


})

function tableData1(data) {

    //console.log(data);

    const output = document.querySelector('tbody');

    output.innerHTML = "";


    data.forEach(data => {

        output.innerHTML += `

                <tr>
                <td>${data.Name}</td>
                <td>${data.Phone}</td>
                <td>${data.Email}</td>
                <td>${data.Address}</td>
                </tr>`;
    });

}